member.data.coop
================

To start developing:

    # Create a virtualenv with python 3
    $ mkvirtualenv -p python3 datacoop

    # Run this make target, which installs all the requirements and sets up a
    # development database.
    $ make dev-setup

To run the Django development server:

    $ python manage.py runserver

Before you push your stuff, run tests:

    $ make test
