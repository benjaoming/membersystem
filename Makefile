# These are just some make targets, expressing how things
# are supposed to be run, but feel free to change them!

dev-setup:
	pip install -r requirements_dev.txt --upgrade
	pip install -r requirements.txt --upgrade
	pre-commit install
	python manage.py migrate
	python manage.py createsuperuser

lint:
	pre-commit run --all

test:
	pytest
