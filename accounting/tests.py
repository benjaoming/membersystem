import pytest
from django.contrib.auth.models import User

from . import models

# @pytest.fixture
# def test():
# do stuff

@pytest.mark.django_db
def test_balance():
    user = User.objects.create_user("test", "lala@adas.com", "1234")
    account = models.Account.objects.create(
        owner=user
    )
    assert account.balance == 0
