from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from . import models


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):

    list_display = ('who', 'description', 'created', 'is_paid',)

    def who(self, instance):
        return instance.user.get_full_name()
    who.short_description = _("Customer")


@admin.register(models.Payment)
class PaymentAdmin(admin.ModelAdmin):

    list_display = ('who', 'description', 'order_id', 'created',)

    def who(self, instance):
        return instance.order.user.get_full_name()
    who.short_description = _("Customer")

    def order_id(self, instance):
        return instance.order.id
    order_id.short_description = _("Order ID")
