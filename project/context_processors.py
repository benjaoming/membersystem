"""Context processors for the membersystem app."""
from django.contrib.sites.shortcuts import get_current_site


def current_site(request):
    """Include the current site in the context."""
    return {
        'site': get_current_site(request)
    }
